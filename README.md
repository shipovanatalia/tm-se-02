**SOFTWARE**
- Windows 10<br/>
- Apache Maven<br/>
<br/>

**TECHNOLOGIES**
- Java JDK 1.8<br/>
- Maven 3<br/>
<br/>

**BUILD APPLICATION WITH MAVEN**<br/>
maven clean install<br/>
<br/>

**RUN APPLICATION**<br/>
java -jar C:\Shipova\Repo\tm-se-02\target\task-manager-1.0-SNAPSHOT.jar<br/>
<br/>

**DEVELOPER**<br/>
Shipova Natalia <nataliefrance@mail.ru>
