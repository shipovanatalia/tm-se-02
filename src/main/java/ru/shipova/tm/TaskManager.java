package ru.shipova.tm;

import java.util.*;

public class TaskManager {
    private Map<String, Task> taskMap;
    private List<String> listOfCommands;

    public TaskManager() {
        this.taskMap = new HashMap<String, Task>();
        this.listOfCommands = new ArrayList<String>();
        listOfCommands.add("task-create: Create new task");
        listOfCommands.add("task-list: Show all tasks");
        listOfCommands.add("task-clear: Remove all tasks");
        listOfCommands.add("task-remove: Remove selected task");
    }

    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");

        String input = App.in.nextLine();
        String id = UUID.randomUUID().toString();
        taskMap.put(id, new Task(id, input));

        System.out.println("[OK]");
    }

    public void list() {
        System.out.println("[TASK LIST]");

        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        int i = 1;

        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            String name = entry.getValue().getName();
            System.out.println(i + ": " + name);
            i++;
        }
    }

    public void clear() {
        taskMap.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public void remove() {
        System.out.println("ENTER NAME:");
        String input = App.in.nextLine();

        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (input.equals(entry.getValue().getName())) {
                iterator.remove();
                System.out.println("[TASK " + input + " REMOVE]");
            }
        }
    }

    public void printAllCommands() {
        for (String command : listOfCommands) {
            System.out.println(command);
        }
    }
}
